@extends('layouts.app')

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">User Profile</div>

                <div class="card-body">
                <h2 style="text-align:center">{{$user->name}}</h2>

                        <div class="card">
                            <img class=" img-responsive img-circle" style="border: 5px solid #d1c5d8;"
                            src="{{asset('uploads/gallery').'/'.$user->image}}"
                            alt="User profile picture" height="250" width="250">


                        <form method="post" action="{{url('/user/'.$user->id)}}" enctype="multipart/form-data">

                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="author">image</label>
                                <input type="file" class="form-control" name="image"/>
                            </div>
                            <input class="btn btn-primary" type="submit" value="submit">
                        </form>
                        <h1>{{$user->name}}</h1>
                        <p class="title">{{$user->address}}</p>
                        <p>Harvard University</p>
                        <div style="margin: 24px 0;">
                            <a href="#"><i class="fa fa-dribbble"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                        </div>
                        <p><button>{{$user->phoneNo}}</button></p>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<form method="post" action="{{url('/user/'.$user->id)}}" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    {{ csrf_field() }}
    <div class="form-group">
      <label for="exampleInputEmail1">Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{$user->name }}" aria-describedby="emailHelp" placeholder="Enter name">

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control" id="email" name="email" value="{{$user->email  }}" aria-describedby="emailHelp" placeholder="Enter email">

      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Address</label>
        <input type="text" class="form-control" id="address" name="address" value="{{$user->address }}" aria-describedby="emailHelp" placeholder="Enter address">

      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Contact number</label>
        <input type="text" class="form-control" id="phoneNo" name="phoneNo"  value="{{$user->phoneNo}}" aria-describedby="emailHelp" placeholder="Enter mobile number">

      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Image </label>
        <img class=" img-responsive img-circle" style="border: 5px solid #d1c5d8;"
                            src="{{asset('uploads/gallery').'/'.$user->image}}"
                            alt="User profile picture" height="50" width="50">
      <input type="file" class="form-control" name="image" />
      </div>


    <button type=" " class="btn btn-primary">Update</button>
    <a href="/change-password" class="btn btn-primary">change password</a>
  </

@endsection
