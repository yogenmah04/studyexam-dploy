@extends('layouts.app')

@section('content')
<form method="post" action="{{url('/tags')}}" accept-charset="UTF-8" enctype="multipart/form-data">
    {{ csrf_field() }}

@include('tags.form')

</form>
@endsection
