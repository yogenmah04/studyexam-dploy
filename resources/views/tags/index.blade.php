@extends('layouts.app')

@section('content')
    <div class="col-md-1-12">
      <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Created By</th>
      <th colspan="2">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($tags as $item)
      <tr>
        <th scope="row">{{ $item->id }}</th>
        <td>{{ $item->name }}</td>
        <td>{{ $item->users->name }}</td>
        <td><a href="tags/{{ $item->id }}/edit">Edit</a></td>
        <td>
          <form method="POST" action="{{url('/tags/'.$item->id )}}" enctype="multipart/form-data">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
        <input type="submit" value="Delete" >
      </form>
    </td>
      </tr>
    @endforeach


  </tbody>
</table>




    </div>


@endsection
