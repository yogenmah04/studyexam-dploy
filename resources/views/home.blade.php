@extends('layouts.app')

@section('content')
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
<div class="col-lg-4 col-md-6 mb-4">
    <div class="card h-100">
        <a href="#"><img class="card-img-top" src="{{asset('uploads/gallery').'/'.$user->image}}" alt=""></a>
        <div class="card-body">
            <h4 class="card-title">
                <a href="#">{{$user->name}}</a>
            </h4>
            <h5>{{$user->phoneNo}}</h5>
            <p class="card-text">{{$user->address}}</p>
        </div>
        <div class="card-footer">
            <a href="/user/profile">Update</a>
        </div>
    </div>
</div>
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">



                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
