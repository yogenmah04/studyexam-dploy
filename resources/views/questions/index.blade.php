@extends('layouts.app')
@section('content')
    <h1>question lists</h1>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">id</th>
            {{-- <th scope="col">User Name</th>  --}}
            <th scope="col">Question</th>
            <th scope="col"> Stored Date</th>
            <th scope="col" colspan="2">Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($questions as $item)
            <tr>
            <th scope="row">{{$item->id}}</th>
                {{-- <td>{{$item->user->name}}</td> --}}
                <td>{{$item->question}}</td>
                <td>{{$item->created_at}}</td>
                <td><a href="/questions/{{ $item->id }}/edit" class="btn-group btn-group-sm">Edit</a></td>
              </tr>
        @endforeach

        </tbody>
      </table>

@endsection
