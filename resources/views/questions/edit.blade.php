@extends('layouts.app')
@section('content')
    <h1> Edit question</h1>
    <form method="post" action="{{url('/questions/'.$data->id)}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
          <div class="form-group green-border-focus ">
            <label for="exampleFormControlTextarea5">Write Questions</label>
          <textarea class="form-control rounded-0" name="question"
           value="{{$data->question}}"
             id="exampleFormControlTextarea5" rows="20" ></textarea>
          </div>

          <input type="submit" value="Update">

    </form>

@endsection
