@extends('layouts.app')
@section('content')
    <h1> create question</h1>
    <form method="post" action="{{url('/questions')}}" enctype="multipart/form-data">
        {{ csrf_field() }}

          <div class="form-group green-border-focus ">
            <label for="exampleFormControlTextarea5">Write Questions</label>
            <textarea class="form-control rounded-0" name="question" id="exampleFormControlTextarea5" rows="20" ></textarea>
          </div>

          <input type="submit" value="create">

    </form>

@endsection
