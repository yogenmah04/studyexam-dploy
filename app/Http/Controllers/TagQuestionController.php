<?php

namespace App\Http\Controllers;

use App\tag_question;
use Illuminate\Http\Request;

class TagQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tag_question  $tag_question
     * @return \Illuminate\Http\Response
     */
    public function show(tag_question $tag_question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tag_question  $tag_question
     * @return \Illuminate\Http\Response
     */
    public function edit(tag_question $tag_question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tag_question  $tag_question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tag_question $tag_question)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tag_question  $tag_question
     * @return \Illuminate\Http\Response
     */
    public function destroy(tag_question $tag_question)
    {
        //
    }
}
