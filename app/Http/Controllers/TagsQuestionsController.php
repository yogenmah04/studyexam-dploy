<?php

namespace App\Http\Controllers;

use App\tags_questions;
use Illuminate\Http\Request;

class TagsQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tags_questions  $tags_questions
     * @return \Illuminate\Http\Response
     */
    public function show(tags_questions $tags_questions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tags_questions  $tags_questions
     * @return \Illuminate\Http\Response
     */
    public function edit(tags_questions $tags_questions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tags_questions  $tags_questions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tags_questions $tags_questions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tags_questions  $tags_questions
     * @return \Illuminate\Http\Response
     */
    public function destroy(tags_questions $tags_questions)
    {
        //
    }
}
