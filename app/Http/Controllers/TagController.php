<?php

namespace App\Http\Controllers;

use App\Repositories\Repository;
use App\Repositories\TagRepository;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;

    public function __construct(Tag $tag){
        $this->middleware('auth');
        $this->model = new TagRepository($tag);
    }

    public function index()
    {
        //
        $tags= $this->model->with(['Users'])->get();
        // $tags=$this->model->with(['users'])->get();
        // $tags=$this->model->all();
        // $tags=Tag::all();
        // dd($tags->toarray());
        return view('tags.index',compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('dashboard');
        return view('tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //      $this->validate($request, [
        //        'body' => 'required|max:500'
        //    ]);
        $id = Auth::user()->id;
        $request['user_id']=$id;
       // create record and pass in only fields that are fillable
        $this->model->create($request->only($this->model->getModel()->fillable));
        return redirect('tags');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    // public function show(Tag $tag)
    // {
    //     //
    //     return $this->model->find($tag);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // dd($tag);
        // $tag=$this->model->show($tag);
        $tag=Tag::findOrFail($id);
        // dd($tag->toArray());
        return view('tags.edit', compact('tag'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        //
        // update model and only pass in the fillable fields
        // dd($tag->toArray());
        $this->model->update($request->only($this->model->getModel()->fillable), $tag->id);
        return redirect('tags');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
      
        $this->model->delete($tag->id);
        return redirect('/tags');
    }
}
