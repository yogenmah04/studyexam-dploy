<?php

namespace App\Http\Controllers;

use App\Question;
use App\Repositories\QuestionsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    Protected $repo;
    public function __construct(Question $question)
    {
        $this->middleware('auth');
        $this->repo= new QuestionsRepository($question);

    }
    public function index()
    {
        //
        $questions=$this->repo->all();
        return view('questions.index',compact('questions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $id = Auth::user()->id;
        $request['user_id'] = $id;
        // create record and pass in only fields that are fillable
        $this->repo->create($request->only($this->repo->getModel()->fillable));
        return back()->with('success', 'Item created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show($request)
    {
        //
        dd($request);
        $data=$this->repo->show($question->id);
        return view('questions.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {

        $data = $this->repo->show($question->id);
        return view('questions.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        //
        $this->repo->update($request->only($this->repo->getModel()->fillable), $question->id);
        return back()->with('success', 'Item Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //
    }
}
