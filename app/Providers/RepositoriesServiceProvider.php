<?php

namespace App\Providers;

use App\Repositories\Interfaces\QuestionInterface;
use App\Repositories\QuestionRepository;
use Illuminate\Support\ServiceProvider;


class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //


    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        // $this->app->bind(QuestionRepository::class, QuestionInterface::class);

    }
}
