<?php

namespace App\Providers;

use App\Repositories\Interfaces\QuestionInterface;
use App\Repositories\QuestionRepository;
use App\Repositories\QuestionsRepository;
use DB;
use Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\support\facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        // Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        DB::listen(function ($query) {
            Log::info(
                $query->sql,
                $query->bindings,
                $query->time
            );
        });

        $this->app->bind(QuestionInterface::class , QuestionsRepository::class) ;
    }
}
