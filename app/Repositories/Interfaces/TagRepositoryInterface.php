<?php

namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;

interface TagRepositoryInterface{
    public function all();
    public function insertUpdate(Request $req);
    public function delete(Request $req);

}
