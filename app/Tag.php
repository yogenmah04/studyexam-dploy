<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Tag extends Model
{
    //
    protected $table = 'tags';
    protected $fillable = ['id','name', 'user_id'];

    /**
     * Get the model that owns the model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Users()
    {
      return $this->belongsTo(User::class,'user_id');
    }

}
