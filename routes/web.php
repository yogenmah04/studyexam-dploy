<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use app\http\controllers\usercontrollers;
use app\http\Controllers\QuestionController;
use app\http\Controllers\TagController;
Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify'=>true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::post('/user', 'usercontrollers@store');
Route::get('/users', 'UserController@index');
Route::get('/user/profile','UserController@profile');
// Route::post('/user/','usercontrollers@updateProfile')->middleware('verified');

Route::resource('user', 'UserController');

Route::get('/change-password', 'UserController@chnagePasswordget');
// Route::put('/change-password', 'UserController@chnagePassword');
Route::post('/change-password', 'UserController@chnagePassword')->name('changePassword');
Route::resource('/questions', 'QuestionController');
Route::get('/user-question', 'QuestionsController@userIndexList');

Route::resource('tags', 'TagController');

