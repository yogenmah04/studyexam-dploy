<?php

use App\Tag;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('users')->insert([
        //     'name' => Str::random(10),
        //     'email' => 'yogenmah04@gmail.com',
        //     'role'=>'Admin',
        //     'address' =>Str::random(10),
        //     'phoneNo' => Str::random(10),
        //     'password' => Hash::make('11111111'),
        //     'email_verified_at'=> now(),
        //     'created_at'=> now(),
        //     'upadated_at'=> now(),

        // ]);

        $items=[
            'id'=>1,
            'name' => 'yogen maharjan',
            'email' => 'yogenmah04@gmail.com',
            'role'=>'Admin',
            'isActive'=>'1',
            'address' =>Str::random(10),
            'phoneNo' => Str::random(10),
            'password' => Hash::make('11111111'),
            'email_verified_at'=> now(),
        ];
        User::create($items);

        // $tag=[
        //     'id'=>1,
        //     'name'=>'apple',
        //     'user_id'=>'1',
        // ];
        // Tag::create($tag);


    }
}
